﻿using RystadEnergy.BLL.Enums;
using RystadEnergy.BLL.Interfaces.Service;
using System;
using System.Collections.Generic;
using RystadEnergy.Dependencies;
using Unity;
using Microsoft.SqlServer.Types;
using System.Data.Entity.SqlServer;
using RystadEnergy.Entities.RESQL;
using System.Data.Entity.Spatial;
using DotSpatial.Data;
using System.Data;

namespace GlobalProvincesUploader
{
    class Program
    {
        private static readonly string _onShoreMatcher = "Onshore";

        private static readonly string _shapeFileLocation = @"\\oseberg\Common\Maps\RE Geodatabase\Outputs\Shapefiles\Global provinces\Global_provinces.shp";

        private static IUnityContainer Container;

        static void Main(string[] args)
        {
            InitializeDomain();

            SqlServerTypes.Utilities.LoadNativeAssemblies(AppDomain.CurrentDomain.BaseDirectory);
            SqlProviderServices.SqlServerTypesAssemblyName = typeof(SqlGeography).Assembly.FullName;

            var service = Container.Resolve<IGlobalProvinceService>();

            UploadData(service);
        }

        private static void UploadData(IGlobalProvinceService service)
        {
            var sf = Shapefile.OpenFile(_shapeFileLocation);

            var fakeGeometries = new List<KeyValuePair<DataRow, IFeature>>();

            var provinces = new List<GlobalProvince>();

            if (sf.Features.Count < 0) return;

            sf.FillAttributes();
 
            foreach (var item in sf.FeatureLookup)
            {
                fakeGeometries.Add(item);

                var geom = DbGeometry.FromText(item.Value.BasicGeometry.ToString(), 4326);

                provinces.Add(new GlobalProvince
                {
                    Country = item.Key[1].ToString(),
                    Geometry = geom,
                    IsOnshore = item.Key[7].ToString().Equals(_onShoreMatcher),
                    Province = item.Key[0].ToString(),
                    Type = (int)item.Key[6]
                });
            }

            service.AddGlobalProvinces(provinces, truncateData: true);
        }
        
        private static void InitializeDomain()
        {
            Container = new UnityContainer();
            Container.InjectDependencies(EApplicationContext.ReUploader, EConnectionScope.PerLifetime);
        }
    }
}
